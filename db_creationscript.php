
<?php
	/*
	This script creates and initializes the database that will store
	the list of shortened URLs together with its actual reference.
	
	Currently using WampServer with MySQL.
	
	Ref: http://www.w3schools.com/php/php_mysql_create_table.asp
	*/

	// constants
	define( 'T_NAME',	'links' );		// table name
	define( 'T_UID',	'uid' );		// column holding unique IDs
	define( 'T_LONG',	'longurl' );	// column name that stores actual URLs
	define( 'T_SHORT',	'shorturl' );	// column name that stores shortened URLs
	
	// database credentials
	$server 	= "localhost";			// server name
	$username	= "piriinzf";			// username to access database
	$password	= "vH_n6TVpDy6Fu";		// password to access database
	$database	= "piriinzf_linkDb";	// database name
	
	$tablename	= T_NAME;				// table name
	$tableLong	= T_LONG;				// column name that stores actual URLs
	$tableShort	= T_SHORT;				// column name that stores shortened URLs
	
	// establish connection to server
	$connection	= new mysqli( $server, $username, $password );
	
	// Returns the connection to the server
	function Connect()
	{
		global $connection, $database;
	
		if ( $connection->connect_error )
			die( "[ERROR]: Connection failed. " . $connection->connect_error );
			
		$connection->select_db( $database );
		
		return $connection;
	}
	
	// Creates a database
	function CreateDatabase()
	{
		global $connection, $database;
		
		// create the MySQL database (if not already existing
		$sql = "CREATE DATABASE IF NOT EXISTS " . $database;
		$connection->select_db( $database );
		
		if ( $connection->query( $sql ) === true )
			return $sql;
		else
			die( "[ERROR]CD(): " . $connection->mysqli_error );
		
		return false;
	}
	
	// Creates a table to hold the shortened URL and its destination
	function CreateTable()
	{
		global $connection, $tablename;
	
		// create table in database (if not already existing)
		$table = "CREATE TABLE IF NOT EXISTS " . $tablename . "(
			uid INT(4) UNSIGNED AUTO_INCREMENT NOT NULL,
			shorturl VARCHAR(100) NOT NULL,
			longurl VARCHAR(255) NOT NULL,
			
			PRIMARY KEY( uid ),
			UNIQUE( shorturl )
		)
		DEFAULT CHARACTER SET utf8,
		COLLATE utf8_general_ci;
		";
		
		if ( $connection->query( $table ) === true )
		{
			//echo ( "Success" );
			return $table;
		}
		else
			echo ( "[ERROR]CT(): " . $connection->error );
			
		return false;
	}
	
	// Creates a database and a table to hold the shortened URL and its destination
	function Init()
	{
		$db = CreateDatabase();
		
		if ( $db )
			CreateTable();
	}
	
	// Closes the connection to the server
	function Close()
	{
		global $connection;
		
		// close connection
		$connection->close();
	}
?>
