
<?php
include_once( "db_creationscript.php" );	// initialize database

class shortener
{
	// This function returns the prefix to be added before any generated codes
	function GetPrefix()
	{
		$prefix = "piri.co?q=";
		
		return $prefix;
	}

	// This function reads the original URL, checks against the database
	// for a match. If there is one, it returns the already-generated link.
	// Else, it attempts to shorten the URL.
	function ShortenURL( $longurl )
	{
		$connection = Connect();
		
		// for more readable queries
		$tName 	= T_NAME;
		$tShort = T_SHORT;
		$tLong	= T_LONG;
		
		$query = "SELECT * FROM $tName WHERE $tLong = '$longurl'";
		
		// check if original URL exists in database
		if ( ( $matches = $connection->query( $query ) ) == true
			&& $connection->affected_rows > 0 )
		{
			$row = $matches->fetch_assoc();			// obtain the entire row for the match
			$shorturl = $row[$tShort];				// obtain value of shortURL
			
			// reuse short URL if original URL has been registered
			// echo( "Found a match: " . $shorturl );
			return $shorturl;
		}
		else
		{
			// for fun, allow mindvalley.com to have special short URL
			if ( $longurl === "mindvalley.com" )
				$newShortURL = $this->GetPrefix() . "awesome";
				
			else if ( $longurl === "www.mindvalley.com" )
				$newShortURL = $this->GetPrefix() . "Awesomee";
				
			else if ( $longurl === "http://www.mindvalley.com" )
				$newShortURL = $this->GetPrefix() . "AWESOMEEE";
		
			else
			{
				// create a new short URL
				$newShortURL = $this->CreateShortURL( $connection );
			}
			
			// insert the new short URL to database
			$query = 
			"INSERT INTO $tName ( $tShort, $tLong )
			VALUES( '$newShortURL', '$longurl')";
			
			if ( $connection->query( $query ) === true )
			{
				// echo ("Successfully matched: '$newShortURL' & '$longurl'");
			}
			else
				die ("[ERROR]: " . $connection->error );
				
			return $newShortURL;
		}
	}
	
	// This function generates a code to be used as the shortened link.
	// If the code already exists, it regenerates a new one.
	function CreateShortURL()
	{	
		$connection = Connect();
	
		$allowedchars = "ABCDEFGHIJKLMNOPQRSTUVWXYZacbdefghijklmnopqrstuvwxyz1234567890";
		$shorturl = "";
		
		// for more readable queries
		$tName 	= T_NAME;
		$tShort = T_SHORT;
		$tLong	= T_LONG;
		
		// generate a 6-character code aka short URL
		for ( $i = 0; $i < 6; $i++ )
			$shorturl .= $allowedchars[rand( 0, strlen( $allowedchars ) - 1)];
		
		// check if the short URL is a duplicate
		$query = "SELECT * FROM $tName WHERE $tShort = " . $shorturl;
		$connection->query( $query );
		
		if ( $connection->affected_rows > 0 )
		{
			// regenerate another short URL if already exists
			CreateShortURL( $connection );
		}
		else
		{
			// REPLACE THIS WITH DOMAIN NAME, MAKE SURE IT'S SHORT
			return $this->GetPrefix() . $shorturl;
		}
	}
	
	// If user provides a shortened URL, search the
	// database for its actual destination.
	function GetShortURLDestination( $shorturl )
	{
		$connection = Connect();
		
		// for more readable queries
		$tName 	= T_NAME;
		$tShort = T_SHORT;
		$tLong	= T_LONG;
	
		$query = "SELECT $tLong FROM $tName WHERE $tShort = '$shorturl'";
		
		/*
			similar to checking if original URL exists, this checks
			if the short URL exists. If it does, obtain the original URL
			and redirect the user using that value.
		*/
		if ( ( $matches = $connection->query( $query ) ) == true
			&& $connection->affected_rows > 0 )
		{
			$row = $matches->fetch_assoc();			// obtain the entire row for the match
			$longurl = $row[$tLong];				// obtain value of longURL
			
			return $longurl;
		}
	}
}
	
?>