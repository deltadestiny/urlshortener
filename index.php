<!DOCTYPE html>

<?php
	include_once( "mv/shortener.php" );			// main chunk of code goes here
	//include_once( "mv/db_creationscript.php" );	// initialize database
	
	// Only required once to set up MySQL database
	// Init();
	
	if ( isset( $_GET['q'] ) && !empty( $_GET['q'] ) )
	{
		$shortener = new shortener;
		$prefix = "piri.co?q=";
		
		$actualURL = $shortener->GetShortURLDestination( $prefix . $_GET['q'] );
		
		if ( !empty( $actualURL ) )
		{
			if ( strpos( $actualURL, "http" ) === false || 
				strpos( $actualURL, "http" ) != 0 )
			{
				$updatedURL = "http://" . $actualURL;
				//header( "Location:$updatedURL");
				echo "
					<script>
						window.open('$updatedURL','_self');
					</script>
				";
			}
			else
			{
				//header( "Location:$actualURL" );
				echo "
					<script>
						window.open('$actualURL','_self');
					</script>
				";
			}
		}
		exit;
	}
?>

<html>
<head>
	<title>URL Shortener</title>
	<link rel="stylesheet" type="text/css" href="mv/stylesheet.css" />
	<script>
		// this function blocks shortened URLs from being accepted
		function checkURL()
		{
			var url = document.getElementById("longurl").value;
			
			if ( url === "" )
			{
				return false;
			}
			else if ( url.indexOf( "piri.co" ) > -1 )
			{
				document.getElementById("error").style.visibility="visible";
				return false;
			}
			
			document.getElementById("error").style.visibility="hidden";
			return true;
		}
	</script>
</head>
<body>
	<div class="wrapper">
		<form id="urlform" method="post" action="mv/bridge.php" onsubmit="return checkURL()" >
			<table>
				<tr>
					<td class="holder" >
						<div class="pointer">URL</div>
					</td>
					<td class="textspace">
						&nbsp;<input name="longurl" id="longurl" type="text" size="50" class="textspace" autofocus />
					</td>
					<td class="button">
						&nbsp;<input name="shorten" type="submit" value="SHORTEN" class="button" />
						<!--<p class="button" onclick="document.getElementById('urlform').submit();">SHORTEN</p>-->
					</td>
				</tr>
				<tr>
					<td colspan="3" class="foot">
						<p id="error" style="visibility: hidden">This link cannot be shortened.</p>
					</td>
				</td>
				<!--
				<tr>
					<td style="text-align: center"><div class="pointer">Link&nbsp;</div></td>
					<td>&nbsp;<input name="destination" type="text" size="50" colspan="2" 
						style="height: 20px; font-size: 120%; padding-left: 10px;" /></td>
					<td>&nbsp;</td>
				</tr>
			-->
			</table>
		</form>
		
		<div class="push"></div>
	</div>
	
	<footer class="footer">
		<p id="footer">Disclaimer: This is URL shortener serves strictly for personal purposes only. It is not a legitimate shortener.</p>
		<p style="text-align: center"><a href="https://bitbucket.org/deltadestiny/urlshortener.git" target="_blank" class="overlink">Source code</a></p
	</footer>
</body>
</html>