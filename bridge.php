
<?php
	/*
	This script will serve as a bridge between the 
	frontpage and the shortener API.
	*/

	include_once("shortener.php");
	
	// longurl = input name obtained from index.php
	// shorten = submit action obtained from index.php, method POST
	
	if ( isset ( $_POST['shorten'] ) 
		&& isset ( $_POST['longurl'] ) 
		&& !empty ( $_POST['longurl'] ) 
		)
	{
		// convert special characters to html-supported format
		$url = htmlspecialchars( $_POST['longurl'] );
		
		// SHORTEN URL HERE
		$shortener = new shortener();
		$shortenedURL = $shortener->ShortenURL( $url );
	}
	else
	{
		header( "Location:../index.php" );
	}
?>

<!DOCTYPE html>

<html>
<head>
	<title>URL Shortener</title>
	<link rel="stylesheet" type="text/css" href="stylesheet.css" />
	<script>
		function redirect()
		{
			var link = document.getElementById("destination").value;
			
			// add 'http' in the beginning for HTTP authentication
			if ( link.indexOf("http://") == -1 )
				link = "http://".concat( link );
			
			window.open( link, "_self" );
		}
	</script>
</head>
<body>
	<div align="center" style="margin-top: 25%">
		<table>
			<tr>
				<td style="text-align: center" class="holder">
					<div class="pointer">Link</div>
				</td>
				<td class="textspace">
					&nbsp;<input name="destination" id="destination" type="text" size="50" 
						class="textspace" autofocus onfocus="this.select();"
						value="<?php echo isset( $shortenedURL ) ? $shortenedURL : ''; ?>"/>
				</td>
				<td class="button">
					&nbsp; <input name="go" type="submit" value="GO" class="button" onclick="redirect()"/>
				</td>
				<!--<td>&nbsp;</td>-->
			</tr>
			<tr>
				<td colspan="3" class="foot">
					<a href="../index.php" class="overlink">shorten another</a>
				</td>
			</tr>
		</table>
	</div>
</body>
</html>